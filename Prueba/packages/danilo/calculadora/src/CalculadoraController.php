<?php

namespace Danilo\Calculadora;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalculadoraController extends Controller
{
    public function suma($a, $b){
    	$result = $a + $b;
    	return view('calculadora::resultado', compact('result'));
    }

    public function resta($a, $b){
    	$result = $a - $b;
    	return view('calculadora::resultado', compact('result'));
    }
}
