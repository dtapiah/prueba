<?php

Route::get('calculadora', function(){
	echo 'Hello from the calculator package!';
});

Route::get('suma/{a}/{b}', 'Danilo\Calculadora\CalculadoraController@suma');
Route::get('resta/{a}/{b}', 'Danilo\Calculadora\CalculadoraController@resta');